
# Examples

```sh
papis add --from pdf2doi <file>
```

```sh
 papis addto -f to_import/Sieve\ Plateau\ Variance\ Estimat.pdf davies
```

Export references:

```sh
papis export --all -f bibtex 'projects: carex'
```

combine PDFs

```sh
pdfunite [options] <PDF-sourcefile-1>..<PDF-sourcefile-n> <PDF-destfile>
```
