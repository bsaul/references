{
  description = "references";
  nixConfig = { bash-prompt = "📖: "; };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils}:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

      in {

        formatter = pkgs.nixfmt;


        devShells.default =
          pkgs.mkShell { buildInputs = [
            pkgs.nixfmt

            # PDF utilities
            pkgs.poppler_utils

            # Reference manager
            pkgs.papis

            # Documentation/writing tools
            pkgs.pandoc
            
         ]; };
      });
}
