abstract: In this article we propose a general framework for normal approximation
  using Stein's method. We introduce the new concept of Stein couplings and we show
  that it lies at the heart of popular approaches such as the local approach, exchangeable
  pairs, size biasing and many other approaches. We prove several theorems with which
  normal approximation for the Wasserstein and Kolmogorov metrics becomes routine
  once a Stein coupling is found. To illustrate the versatility of our framework we
  give applications in Hoeffding's combinatorial central limit theorem, functionals
  in the classic occupancy scheme, neighbourhood statistics of point patterns with
  fixed number of points and functionals of the components of randomly chosen vertices
  of sub-critical Erdos-Renyi random graphs. In all these cases, we use new, non-standard
  couplings.
archiveprefix: arXiv
author: Chen, Louis H. Y. and Röllin, Adrian
author_list:
- family: Chen
  given: Louis H. Y.
- family: Röllin
  given: Adrian
eprint: 1003.6039v2
file: 1003.6039v2.pdf
files:
- chen2010.pdf
month: Mar
papis_id: 2e1e7abb0dfcc7767b3ee9766eceeb0c
primaryclass: math.PR
ref: Chen2010steinCoup
time-added: 2023-08-11-11:25:32
title: Stein couplings for normal approximation
type: article
url: http://arxiv.org/abs/1003.6039v2
year: '2010'
tags:
- stein's method
projects:
- carpinus
