author: Takeshita, Kazutaka M. and Hayashi, Takehiko I. and Yokomizo, Hiroyuki
author_list:
- affiliation: []
  family: Takeshita
  given: Kazutaka M.
- affiliation: []
  family: Hayashi
  given: Takehiko I.
- affiliation: []
  family: Yokomizo
  given: Hiroyuki
citations:
- article-title: An introduction to propensity score methods for reducing the effects
    of confounding in observational studies
  author: Austin
  doi: 10.1080/00273171.2011.568786
  first-page: '399'
  journal-title: Multivariate Behav. Res.
  volume: '46'
  year: '2011'
- article-title: Pesticides reduce regional biodiversity of stream invertebrates
  author: Beketov
  doi: 10.1073/pnas.1305618110
  first-page: '11039'
  journal-title: Proc. Natl. Acad. Sci. U.S.A.
  volume: '110'
  year: '2013'
- article-title: 'Evidence-based public health: a fundamental concept for public health
    practice'
  author: Brownson
  doi: 10.1146/annurev.publhealth.031308.100134
  first-page: '175'
  journal-title: Annu. Rev. Publ. Health
  volume: '30'
  year: '2009'
- article-title: Sensitivity and variability of metrics used in biological assessments
    of running waters
  author: Carlisle
  doi: 10.1002/etc.5620180227
  first-page: '285'
  journal-title: Environ. Toxicol. Chem.
  volume: '18'
  year: '1999'
- article-title: Assessing sensitivity to unmeasured confounding using a simulated
    potential confounder
  author: Carnegie
  first-page: '395'
  journal-title: J. Res. Educ. Effect.
  volume: '9'
  year: '2016'
- article-title: Benthic invertebrate community responses to heavy metals in the upper
    Arkansas River basin, Colorado
  author: Clements
  doi: 10.2307/1467263
  first-page: '30'
  journal-title: J. North Am. Benthol. Soc.
  volume: '13'
  year: '1994'
- article-title: Heavy metals structure benthic communities in Colorado mountain streams
  author: Clements
  doi: 10.1890/1051-0761(2000)010[0626:HMSBCI]2.0.CO;2
  first-page: '626'
  journal-title: Ecol. Appl.
  volume: '10'
  year: '2000'
- article-title: Use of field data to support European Water Framework Directive quality
    standards for dissolved metals
  author: Crane
  doi: 10.1021/es0629460
  first-page: '5014'
  journal-title: Environ. Sci. Technol.
  volume: '41'
  year: '2007'
- article-title: A novel method for predicting chronic nickel bioavailability and
    toxicity to Daphnia magna in artificial and natural waters
  author: Deleebeeck
  doi: 10.1897/07-579.1
  first-page: '2097'
  journal-title: Environ. Toxicol. Chem.
  volume: '27'
  year: '2008'
- author: European Commission
  series-title: 'European Union Risk Assessment Report: Nickel and Nickel Compounds'
  year: '2008'
- article-title: On the specification of structural equation models for ecological
    systems
  author: Grace
  doi: 10.1890/09-0464.1
  first-page: '67'
  journal-title: Ecol. Monogr.
  volume: '80'
  year: '2010'
- article-title: Causal diagrams for epidemiologic research
  author: Greenland
  doi: 10.1097/00001648-199901000-00008
  first-page: '37'
  journal-title: Epidemiology
  volume: '10'
  year: '1999'
- article-title: Occurrence of neonicotinoids and fipronil in estuaries and their
    potential risks to aquatic invertebrates
  author: Hano
  doi: 10.1016/j.envpol.2019.05.067
  first-page: '205'
  journal-title: Environ. Pollut.
  volume: '252'
  year: '2019'
- author: Hernán
  series-title: 'Causal Inference: what if'
  year: '2020'
- article-title: 'Comparing macroinvertebrate assemblages at organic-contaminated
    river sites with different zinc concentrations: metal-sensitive taxa may already
    be absent'
  author: Iwasaki
  doi: 10.1016/j.envpol.2018.05.041
  first-page: '272'
  journal-title: Environ. Pollut.
  volume: '241'
  year: '2018'
- article-title: Effects of heavy metals on riverine benthic macroinvertebrate assemblages
    with reference to potential food availability for drift-feeding fishes
  author: Iwasaki
  doi: 10.1897/08-200.1
  first-page: '354'
  journal-title: Environ. Toxicol. Chem.
  volume: '28'
  year: '2009'
- article-title: 'Effect of zinc on diversity of riverine benthic macroinvertebrates:
    estimation of safe concentrations from field data'
  author: Iwasaki
  doi: 10.1002/etc.612
  first-page: '2237'
  journal-title: Environ. Toxicol. Chem.
  volume: '30'
  year: '2011'
- article-title: Estimating safe concentrations of trace metals from inter-continental
    field data on river macroinvertebrates
  author: Iwasaki
  doi: 10.1016/j.envpol.2012.03.028
  first-page: '182'
  journal-title: Environ. Pollut.
  volume: '166'
  year: '2012'
- article-title: Quantifying differences in responses of aquatic insects to trace
    metal exposure in field studies and short-term stream mesocosm experiments
  author: Iwasaki
  doi: 10.1021/acs.est.7b06628
  first-page: '4378'
  journal-title: Environ. Sci. Technol.
  volume: '52'
  year: '2018'
- article-title: Aquatic insect larvae as indicators of limiting minimal contents
    of dissolved oxygen – Part II
  author: Jacob
  doi: 10.1080/01650428409361183
  first-page: '185'
  journal-title: Aquat. Insects
  volume: '3'
  year: '1984'
- article-title: Impact of watershed urbanization on stream insect communities
  author: Jones
  doi: 10.1111/j.1752-1688.1987.tb00854.x
  first-page: '1047'
  journal-title: Water Resour. Bull.
  volume: '23'
  year: '1987'
- article-title: Acute and chronic toxicity of nickel to a cladoceran (Ceriodaphnia
    dubia) and an amphipod (Hyalella azteca)
  author: Keithly
  doi: 10.1897/02-630
  first-page: '691'
  journal-title: Environ. Toxicol. Chem.
  volume: '23'
  year: '2004'
- author: Lenth
- article-title: Validation trial of Japan’s zinc water quality standard for aquatic
    life using field data
  author: Matsuzaki
  doi: 10.1016/j.ecoenv.2011.07.003
  first-page: '1808'
  journal-title: Ecotoxicol. Environ. Saf.
  volume: '74'
  year: '2011'
- article-title: Recovery of a mining-damaged stream ecosystem
  author: Mebane
  doi: 10.12952/journal.elementa.000042
  journal-title: Elem. Sci. Anth.
  volume: '3'
  year: '2015'
- article-title: Bioaccumulation and toxicity of cadmium, copper, nickel, and zinc
    and their mixtures to aquatic insect communities
  author: Mebane
  doi: 10.1002/etc.4663
  first-page: '812'
  journal-title: Environ. Toxicol. Chem.
  volume: '39'
  year: '2020'
- author: Ministry of the Environment of Japan
  series-title: Report on the Research for the Consideration of the Environmental
    Standards for the Conservation of Aquatic Organisms
  year: '2017'
- article-title: Critical oxygen demand in Plecoptera and Ephemeroptera nymphs as
    determined by two methods
  author: Nagell
  doi: 10.2307/3544382
  first-page: '75'
  journal-title: OIKOS
  volume: '36'
  year: '1981'
- article-title: 'Comment: graphical models, causality and intervention'
  author: Pearl
  doi: 10.1214/ss/1177010894
  first-page: '266'
  journal-title: Stat. Sci.
  volume: '8'
  year: '1993'
- author: Pearl
  series-title: 'Causality: Models, Reasoning, and Inference'
  year: '2009'
- author: Pearl
  series-title: 'Causal Inference in Statistics: A Primer'
  year: '2016'
- author: Pearl
  series-title: 'The Book of Why: the New Science of Cause and Effect'
  year: '2018'
- article-title: Validation of the nickel biotic ligand model for locally relevant
    species in Australian freshwaters
  author: Peters
  doi: 10.1002/etc.4213
  first-page: '2566'
  journal-title: Environ. Toxicol. Chem.
  volume: '37'
  year: '2018'
- article-title: Assessment of the effects of nickel on benthic macroinvertebrates
    in the field
  author: Peters
  doi: 10.1007/s11356-013-1851-2
  first-page: '193'
  journal-title: Environ. Sci. Pollut. Res.
  volume: '21'
  year: '2014'
- article-title: Four reasons why traditional metal toxicity testing with aquatic
    insects is irrelevant
  author: Poteat
  doi: 10.1021/es405529n
  first-page: '887'
  journal-title: Environ. Sci. Technol.
  volume: '48'
  year: '2014'
- article-title: Acute and chronic toxicity of nickel to larvae of Chironomus riparis
    (Meigen)
  author: Powlesland
  doi: 10.1016/0143-1471(86)90044-9
  first-page: '47'
  journal-title: Environ. Pollut. (Ser. A)
  volume: '42'
  year: '1986'
- article-title: 'Using propensity scores for causal inference in ecology: options,
    considerations, and a case study'
  author: Ramsey
  doi: 10.1111/2041-210X.13111
  first-page: '320'
  journal-title: Methods Ecol. Evol.
  volume: '10'
  year: '2019'
- author: R Core Team
  series-title: 'R: A Language and Environment for Statistical Computing'
  year: '2018'
- article-title: Cross-species extrapolation of chronic nickel biotic ligand models
  author: Schlekat
  doi: 10.1016/j.scitotenv.2010.09.012
  first-page: '6148'
  journal-title: Sci. Total Environ.
  volume: '408'
  year: '2010'
- article-title: Development of a new toxic-unit model for the bioassessment of metals
    in streams
  author: Schmidt
  doi: 10.1002/etc.302
  first-page: '2432'
  journal-title: Environ. Toxicol. Chem.
  volume: '29'
  year: '2010'
- article-title: Linking the agricultural landscape of the Midwest to stream health
    with structural equation modeling
  author: Schmidt
  doi: 10.1021/acs.est.8b04381
  first-page: '452'
  journal-title: Environ. Sci. Technol.
  volume: '53'
  year: '2019'
- article-title: 'Population marginal means in the linear model: an alternative to
    least squares means'
  author: Searle
  first-page: '216'
  journal-title: Am. Statistician
  volume: '35'
  year: '1980'
- article-title: 'Toxicity of proton–metal mixtures in the field: linking stream macroinvertebrate
    species diversity to chemical speciation and bioavailability'
  author: Stockdale
  doi: 10.1016/j.aquatox.2010.07.018
  first-page: '112'
  journal-title: Aquat. Toxicol.
  volume: '100'
  year: '2010'
- article-title: 'Associations of community structure and functions of benthic invertebrates
    with nickel concentrations: analyses from field surveys'
  author: Takeshita
  doi: 10.1002/etc.4462
  first-page: '1728'
  journal-title: Environ. Toxicol. Chem.
  volume: '38'
  year: '2019'
- article-title: TOC evaluation of public waterbody in Fukuoka Prefecture and use
    of TOD/BOD value for water quality conservation
  author: Tanaka
  first-page: '246'
  journal-title: J. Environ. Lab. Assoc.
  volume: '34'
  year: '2009'
- article-title: WHAM—a chemical equilibrium model and computer code for waters, sediments,
    and soils incorporating a discrete site/electrostatic model of ion-binding by
    humic substances
  author: Tipping
  doi: 10.1016/0098-3004(94)90038-8
  first-page: '973'
  journal-title: Comput. Geosci.
  volume: '20'
  year: '1994'
- article-title: Use of a macroinvertebrate based biotic index to estimate critical
    metal concentrations for good ecological water quality
  author: Van Ael
  doi: 10.1016/j.chemosphere.2014.06.001
  first-page: '138'
  journal-title: Chemosphere
  volume: '119'
  year: '2015'
- article-title: Using propensity scores to estimate the effects of insecticides on
    stream invertebrates from observational data
  author: Yuan
  doi: 10.1897/08-551.1
  first-page: '1518'
  journal-title: Environ. Toxicol. Chem.
  volume: '28'
  year: '2009'
doi: 10.1016/j.envpol.2020.115059
files:
- takeshita2020-a.pdf
journal: Environmental Pollution
language: en
month: 10
pages: '115059'
papis_id: d74825cb3747c80e33aeb50e7f01b0d2
publisher: Elsevier BV
ref: Takeshita2020
time-added: 2023-08-04-08:18:06
title: 'The effect of intervention in nickel concentrations on benthic macroinvertebrates:
  A case study of statistical causal inference in ecotoxicology'
type: article
url: http://dx.doi.org/10.1016/j.envpol.2020.115059
volume: '265'
year: 2020
tags:
- causal inference
- ecology
projects:
- tridens
