abstract: In observational causal inference, in order to emulate a randomized experiment,
  weights are used to render treatments independent of observed covariates. This property
  is known as balance; in its absence, estimated causal effects may be arbitrarily
  biased. In this work we introduce permutation weighting, a method for estimating
  balancing weights using a standard binary classifier (regardless of cardinality
  of treatment). A large class of probabilistic classifiers may be used in this method;
  the choice of loss for the classifier implies the particular definition of balance.
  We bound bias and variance in terms of the excess risk of the classifier, show that
  these disappear asymptotically, and demonstrate that our classification problem
  directly minimizes imbalance. A wide variety of existing balancing weights may be
  estimated through this regime, allowing for direct comparison between methods based
  on classifier loss, as well as hyper-parameter tuning using cross-validation. Empirical
  evaluations indicate that permutation weighting provides favorable performance in
  comparison to existing methods.
archiveprefix: arXiv
author: Arbour, David and Dimmery, Drew and Sondhi, Arjun
author_list:
- family: Arbour
  given: David
- family: Dimmery
  given: Drew
- family: Sondhi
  given: Arjun
eprint: 1901.01230v4
file: 1901.01230v4.pdf
files:
- arbour2019.pdf
month: Jan
papis_id: 68e474e8622dab38c1447dcf664a7d0b
primaryclass: stat.ME
ref: Arbour2019permutatio
time-added: 2023-08-10-08:49:15
title: Permutation Weighting
type: article
url: http://arxiv.org/abs/1901.01230v4
year: '2019'
tags:
- causal inference
projects:
- impatiens
