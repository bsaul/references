author: Hill, Jonathan B.
author_list:
- affiliation: []
  family: Hill
  given: Jonathan B.
citations:
- article-title: A characterization of multivariate regular variation
  author: Basrak
  doi: 10.1214/aoap/1031863174
  first-page: '908'
  journal-title: Ann. Appl. Prob.
  volume: '12'
  year: '2002'
- article-title: Extremes in non-life insurance
  author: Beirlant
  series-title: Extreme Value Theory and Applications
  year: '1994'
- article-title: Convergence criteria for multiparameter stochastic processes and
    some applications
  author: Bickel
  doi: 10.1214/aoms/1177693164
  first-page: '1656'
  journal-title: Ann. Math. Statist.
  volume: '42'
  year: '1971'
- author: Billingsley
  series-title: Convergence of Probability Measures
  year: '1999'
- author: Bingham
  series-title: Regular Variation
  year: '1987'
- article-title: Financial risk and heavy tails
  author: Bradley
  series-title: Handbook of Heavy Tailed Distributions in Finance
  year: '2003'
- article-title: Mixing and moment properties of various GARCH and stochastic volatility
    models
  author: Carrasco
  doi: 10.1017/S0266466602181023
  first-page: '17'
  journal-title: Econometric Theory
  volume: '18'
  year: '2002'
- article-title: Dependence measures for extreme value analysis
  author: Coles
  doi: 10.1023/A:1009963131610
  first-page: '339'
  journal-title: Extremes
  volume: '2'
  year: '1999'
- article-title: Weighted empirical and quantile processes
  author: Csörgö
  doi: 10.1214/aop/1176992617
  first-page: '31'
  journal-title: Ann. Probab.
  volume: '14'
  year: '1986'
- article-title: The central limit theorem for globally non-stationary near-epoch-dependent
    functions of mixing processes
  author: Davidson
  doi: 10.1017/S0266466600012950
  first-page: '313'
  journal-title: Econometric Theory
  volume: '8'
  year: '1992'
- author: Davidson
  series-title: Stochastic Limit Theory
  year: '1994'
- article-title: Moment and Memory Properties of Linear Conditional Heteroscedasticity
    Models, and a New Model
  author: Davidson
  doi: 10.1198/073500103288619359
  first-page: '16'
  journal-title: J. Bus. Econ. Statist.
  volume: '22'
  year: '2004'
- article-title: The functional central limit theorem and weak convergence to stochastic
    integrals
  author: Davidson
  doi: 10.1017/S0266466600165028
  first-page: '621'
  journal-title: Econometric Theory
  volume: '16'
  year: '2000'
- article-title: Central limit theorems for dependent heterogeneous random variables
  author: de Jong
  doi: 10.1017/S0266466600005843
  first-page: '353'
  journal-title: Econometric Theory
  volume: '13'
  year: '1997'
- author: Dellacherie
  series-title: Probabilities and Potential
  year: '1978'
- article-title: Tail empirical processes under mixing conditions
  author: Drees
  series-title: Empirical Process Techniques for Dependent Data
  year: '2002'
- article-title: On maximum likelihood estimation of the extreme value index
  author: Drees
  doi: 10.1214/105051604000000279
  first-page: '1179'
  journal-title: Ann. Appl. Probab.
  volume: '14'
  year: '2004'
- article-title: Limit theorems for tail processes with application to intermediate
    quantile estimation
  author: Einmahl
  doi: 10.1016/0378-3758(92)90156-M
  first-page: '127'
  journal-title: J. Statist. Plann. Inference
  volume: '32'
  year: '1992'
- article-title: Asymptotic normality of extreme value estimators on C[0,1]
  author: Einmahl
  doi: 10.1214/009053605000000831
  first-page: '469'
  journal-title: Ann. Statist.
  volume: '34'
  year: '2006'
- author: Embrechts
  series-title: Modelling Extremal Events for Insurance and Finance
  year: '1997'
- article-title: Modelling dependence with copulas and applications to risk management
  author: Embrechts
  series-title: Handbook of Heavy Tailed Distributions in Finance
  year: '2003'
- doi: 10.1201/9780203483350
  unstructured: Finkenstadt, B., Rootzén, H., 2003. Extreme Values in Finance, Telecommunications
    and the Environment. Chapman & Hall, London.
- unstructured: 'Gallant, A.R., White, H., 1988. A Unified Theory of Estimation and
    Inference for Nonlinear Dynamic Models. Basil Blackwell: Oxford.'
- article-title: 'Slow variation with remainder: theory and applications'
  author: Goldie
  doi: 10.1093/qmath/38.1.45
  first-page: '45'
  journal-title: Quart. J. Math.
  volume: '38'
  year: '1987'
- article-title: On asymptotic normality of Hill's estimator for the exponent of regular
    variation
  author: Haeusler
  doi: 10.1214/aos/1176349551
  first-page: '743'
  journal-title: Ann. Statist.
  volume: '13'
  year: '1985'
- article-title: On Some Estimates of an Exponent of Regular Variation
  author: Hall
  first-page: '37'
  journal-title: J. Roy. Statisti. Soc. Ser. B
  volume: '44'
  year: '1982'
- author: Hall
  series-title: Martingale Limit Theory and its Applications
  year: '1980'
- article-title: Adaptive estimates of parameters of regular variation
  author: Hall
  doi: 10.1214/aos/1176346596
  first-page: '331'
  journal-title: Ann. Statist.
  volume: '13'
  year: '1985'
- article-title: A conditional approach to multivariate extreme values
  author: Heffernan
  doi: 10.1111/j.1467-9868.2004.02050.x
  first-page: '497'
  journal-title: J. Roy. Statist. Soc. Ser. B
  volume: '66'
  year: '2004'
- article-title: A simple general approach to inference about the tail of a distribution
  author: Hill
  doi: 10.1214/aos/1176343247
  first-page: '1163'
  journal-title: Ann. Math. Statist.
  volume: '3'
  year: '1975'
- unstructured: 'Hill, J.B., 2005. On tail index estimation for dependent, heterogenous
    data. Working paper, Department of Economics, University of North Carolina, Chapel
    Hill. Available at: 〈www.unc.edu/∼jbhill/hill_het_dep.pdf〉.'
- unstructured: 'Hill, J.B., 2007a. Technical appendix for “On functional central
    limit theorems for dependent, heterogenous arrays with applications to tail index
    and tail dependence estimation”. Available at: 〈www.unc.edu/∼jbhill/tech_append_tail_fclt.pdf〉.'
- unstructured: 'Hill, J.B., 2007b. Robust non-parametric tests of extremal dependence.
    Working paper, Department of Economics, University of North Carolina, Chapel Hill.
    Available at: 〈www.unc.edu/∼jbhill/extremal_spillover.pdf〉.'
- unstructured: 'Hill, J.B., 2008a. Extremal memory of stochastic volatility with
    applications to tail shape and tail dependence inference. Working paper, Department
    of Economics, University of North Carolina, Chapel Hill. Available at: 〈www.unc.edu/∼jbhill/stoch_vol_tails.pdf〉.'
- unstructured: 'Hill, J.B., 2008b. Tail and non-tail memory with applications to
    distributed lags and central limit theory. Working paper, Department of Economics,
    University of North Carolina, Chapel Hill. Available at: 〈www.unc.edu/∼jbhill/stoch_vol_tails.pdf〉.'
- article-title: On tail index estimation using dependent data
  author: Hsing
  doi: 10.1214/aos/1176348261
  first-page: '1547'
  journal-title: Ann. Statist.
  volume: '19'
  year: '1991'
- article-title: On the asymptotic distributions of partial sums of functionals of
    infinite-variance moving averages
  author: Hsing
  doi: 10.1214/aop/1022677460
  first-page: '1579'
  journal-title: Ann. Probab.
  volume: '27'
  year: '1999'
- article-title: Some limit theorems for stationary processes
  author: Ibragimov
  doi: 10.1137/1107036
  first-page: '349'
  journal-title: Theory Probab. Appl.
  volume: '7'
  year: '1962'
- author: Ibragimov
  series-title: Independent and Stationary Sequences of Random Variables
  year: '1971'
- article-title: Estimating the tail dependence function of an elliptical distribution
  author: Klüppleberg
  doi: 10.3150/07-BEJ6047
  first-page: '229'
  journal-title: Bernoulli
  volume: '13'
  year: '2007'
- article-title: Extremes and local dependence in stationary sequences
  author: Leadbetter
  doi: 10.1007/BF00532484
  first-page: '291'
  journal-title: Z. Wahrsch. Verw. Geb
  volume: '65'
  year: '1983'
- article-title: On central limit theory for families of strongly mixing additive
    set functions
  author: Leadbetter
  first-page: '211'
  series-title: Festschrift in Honour of Gopinath Kallianpur
  year: '1993'
- author: Leadbetter
  series-title: Extremes and Related Properties of Random Sequences and Processes
  year: '1983'
- article-title: Modeling dependence within joint tail regions
  author: Ledford
  doi: 10.1111/1467-9868.00080
  first-page: '475'
  journal-title: J. Roy. Statist. Soc. Ser. B
  volume: '59'
  year: '1997'
- article-title: Diagnostics for dependence within time series extremes
  author: Ledford
  doi: 10.1111/1467-9868.00400
  first-page: '521'
  journal-title: J. Roy. Statist. Soc. Ser. B
  volume: '65'
  year: '2003'
- article-title: A strong invariance theorem for the tail empirical process
  author: Mason
  first-page: '491'
  journal-title: Ann. Inst. H. Poincare
  volume: '24'
  year: '1988'
- article-title: Dependent central limit theorems
  author: McLeish
  doi: 10.1214/aop/1176996608
  first-page: '620'
  journal-title: Ann. Probab.
  volume: '2'
  year: '1974'
- article-title: A maximal inequality and dependent strong laws
  author: McLeish
  doi: 10.1214/aop/1176996269
  first-page: '829'
  journal-title: Ann. Probab.
  volume: '3'
  year: '1975'
- article-title: Stability of nonlinear AR-GARCH models
  author: Meitz
  doi: 10.1111/j.1467-9892.2007.00562.x
  first-page: '453'
  journal-title: J. Time Ser. Anal.
  volume: '29'
  year: '2008'
- article-title: On weak convergence of stochastic processes with multidimensional
    time parameter
  author: Neuhas
  doi: 10.1214/aoms/1177693241
  first-page: '1285'
  journal-title: Ann. Math. Statist.
  volume: '42'
  year: '1971'
- article-title: Multiple time series regression with integrated processes
  author: Phillips
  doi: 10.2307/2297602
  first-page: '473'
  journal-title: Rev. Econom. Stud.
  volume: '53'
  year: '1986'
- article-title: Central limit theorems for partial sums of bounded functionals of
    infinite-variance moving averages
  author: Pipiras
  doi: 10.3150/bj/1066418880
  first-page: '833'
  journal-title: Bernoulli
  volume: '9'
  year: '2003'
- article-title: Bounds for the covariance of functions of infinite variance stable
    random variables with applications to central limit theorems and wavelet-based
    estimation
  author: Pipiras
  doi: 10.3150/07-BEJ6143
  first-page: '1091'
  journal-title: Bernoulli
  volume: '13'
  year: '2007'
- author: Resnick
  series-title: Extreme Values, Regular Variation and Point Processes
  year: '1987'
- unstructured: Rootzén, H., 1995. The tail empirical process for stationary sequences.
    Report 1995:9, Department of Mathematical Statistics, Chalmers University of Technology.
- article-title: On the distribution of tail array sums for strongly mixing stationary
    sequences
  author: Rootzén
  doi: 10.1214/aoap/1028903454
  first-page: '868'
  journal-title: Ann. Probab.
  volume: '8'
  year: '1998'
- article-title: Non-parametric estimation of tail dependence
  author: Schmidt
  doi: 10.1111/j.1467-9469.2005.00483.x
  first-page: '307'
  journal-title: Scand. J. Statist.
  volume: '33'
  year: '2006'
- article-title: The invariance principle for linear processes with applications
  author: Wang
  doi: 10.1017/S0266466602181072
  first-page: '119'
  journal-title: Econometric Theory
  volume: '18'
  year: '2002'
- article-title: Inequalities with applications to the weak convergence of random
    processes with multidimensional time parameters
  author: Wichura
  doi: 10.1214/aoms/1177697741
  first-page: '681'
  journal-title: Ann. Math. Statist.
  volume: '40'
  year: '1969'
- article-title: Some invariance principles and central limit theorems for dependent
    heterogeneous processes
  author: Wooldridge
  doi: 10.1017/S0266466600012032
  first-page: '210'
  journal-title: Econometric Theory
  volume: '4'
  year: '1988'
- article-title: On linear processes with dependent innovations
  author: Wu
  doi: 10.1016/j.spa.2005.01.001
  first-page: '939'
  journal-title: Stochastic Process. Appl.
  volume: '115'
  year: '2005'
- article-title: Martingale approximations for sums of stationary processes
  author: Wu
  doi: 10.1214/009117904000000351
  first-page: '1674'
  journal-title: Ann. Probab.
  volume: '32'
  year: '2004'
doi: 10.1016/j.jspi.2008.09.005
files:
- hill2009-a.pdf
issue: '6'
journal: Journal of Statistical Planning and Inference
language: en
month: 6
pages: 2091--2110
papis_id: bd1040072efbf873e5e12d14d983bc85
publisher: Elsevier BV
ref: Hill2009
time-added: 2023-08-04-08:44:08
title: On functional central limit theorems for dependent, heterogeneous arrays with
  applications to tail index and tail dependence estimation
type: article
url: http://dx.doi.org/10.1016/j.jspi.2008.09.005
volume: '139'
year: 2009
tags:
- central limit theorem
- dependent data
projects:
- carpinus
